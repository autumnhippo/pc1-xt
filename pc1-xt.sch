<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="Thomas">
<packages>
<package name="CARDEDGE_FEMALE_62PINS_8BITISA_STRAIGHT">
<pad name="B1" x="2.54" y="-1.27" drill="0.75"/>
<pad name="B2" x="2.54" y="-3.81" drill="0.75"/>
<pad name="B3" x="2.54" y="-6.35" drill="0.75"/>
<pad name="B4" x="2.54" y="-8.89" drill="0.75"/>
<pad name="B5" x="2.54" y="-11.43" drill="0.75"/>
<pad name="B6" x="2.54" y="-13.97" drill="0.75"/>
<pad name="B7" x="2.54" y="-16.51" drill="0.75"/>
<pad name="B8" x="2.54" y="-19.05" drill="0.75"/>
<pad name="B9" x="2.54" y="-21.59" drill="0.75"/>
<pad name="B10" x="2.54" y="-24.13" drill="0.75"/>
<pad name="B11" x="2.54" y="-26.67" drill="0.75"/>
<pad name="B12" x="2.54" y="-29.21" drill="0.75"/>
<pad name="B13" x="2.54" y="-31.75" drill="0.75"/>
<pad name="B14" x="2.54" y="-34.29" drill="0.75"/>
<pad name="B15" x="2.54" y="-36.83" drill="0.75"/>
<pad name="B16" x="2.54" y="-39.37" drill="0.75"/>
<pad name="B17" x="2.54" y="-41.91" drill="0.75"/>
<pad name="B18" x="2.54" y="-44.45" drill="0.75"/>
<pad name="B19" x="2.54" y="-46.99" drill="0.75"/>
<pad name="B20" x="2.54" y="-49.53" drill="0.75"/>
<pad name="B21" x="2.54" y="-52.07" drill="0.75"/>
<pad name="B22" x="2.54" y="-54.61" drill="0.75"/>
<pad name="B23" x="2.54" y="-57.15" drill="0.75"/>
<pad name="B24" x="2.54" y="-59.69" drill="0.75"/>
<pad name="B25" x="2.54" y="-62.23" drill="0.75"/>
<pad name="B26" x="2.54" y="-64.77" drill="0.75"/>
<pad name="B27" x="2.54" y="-67.31" drill="0.75"/>
<pad name="B28" x="2.54" y="-69.85" drill="0.75"/>
<pad name="B29" x="2.54" y="-72.39" drill="0.75"/>
<pad name="B30" x="2.54" y="-74.93" drill="0.75"/>
<pad name="B31" x="2.54" y="-77.47" drill="0.75"/>
<pad name="A1" x="7.62" y="-1.27" drill="0.75"/>
<pad name="A2" x="7.62" y="-3.81" drill="0.75"/>
<pad name="A3" x="7.62" y="-6.35" drill="0.75"/>
<pad name="A4" x="7.62" y="-8.89" drill="0.75"/>
<pad name="A5" x="7.62" y="-11.43" drill="0.75"/>
<pad name="A6" x="7.62" y="-13.97" drill="0.75"/>
<pad name="A7" x="7.62" y="-16.51" drill="0.75"/>
<pad name="A8" x="7.62" y="-19.05" drill="0.75"/>
<pad name="A9" x="7.62" y="-21.59" drill="0.75"/>
<pad name="A10" x="7.62" y="-24.13" drill="0.75"/>
<pad name="A11" x="7.62" y="-26.67" drill="0.75"/>
<pad name="A12" x="7.62" y="-29.21" drill="0.75"/>
<pad name="A13" x="7.62" y="-31.75" drill="0.75"/>
<pad name="A14" x="7.62" y="-34.29" drill="0.75"/>
<pad name="A15" x="7.62" y="-36.83" drill="0.75"/>
<pad name="A16" x="7.62" y="-39.37" drill="0.75"/>
<pad name="A17" x="7.62" y="-41.91" drill="0.75"/>
<pad name="A18" x="7.62" y="-44.45" drill="0.75"/>
<pad name="A19" x="7.62" y="-46.99" drill="0.75"/>
<pad name="A20" x="7.62" y="-49.53" drill="0.75"/>
<pad name="A21" x="7.62" y="-52.07" drill="0.75"/>
<pad name="A22" x="7.62" y="-54.61" drill="0.75"/>
<pad name="A23" x="7.62" y="-57.15" drill="0.75"/>
<pad name="A24" x="7.62" y="-59.69" drill="0.75"/>
<pad name="A25" x="7.62" y="-62.23" drill="0.75"/>
<pad name="A26" x="7.62" y="-64.77" drill="0.75"/>
<pad name="A27" x="7.62" y="-67.31" drill="0.75"/>
<pad name="A28" x="7.62" y="-69.85" drill="0.75"/>
<pad name="A29" x="7.62" y="-72.39" drill="0.75"/>
<pad name="A30" x="7.62" y="-74.93" drill="0.75"/>
<pad name="A31" x="7.62" y="-77.47" drill="0.75"/>
<rectangle x1="0.635" y1="-81.915" x2="9.635" y2="3.285" layer="39"/>
<wire x1="1.27" y1="0" x2="8.89" y2="0" width="0.127" layer="21"/>
<wire x1="8.89" y1="0" x2="8.89" y2="-78.74" width="0.127" layer="21"/>
<wire x1="8.89" y1="-78.74" x2="1.27" y2="-78.74" width="0.127" layer="21"/>
<wire x1="1.27" y1="-78.74" x2="1.27" y2="0" width="0.127" layer="21"/>
<text x="11.43" y="-19.05" size="1.27" layer="21" rot="R90">&gt;NAME</text>
<text x="1.016" y="-1.27" size="1.27" layer="21" align="center-right">B1</text>
<text x="1.016" y="-77.47" size="1.27" layer="21" align="center-right">B31</text>
<text x="9.144" y="-1.27" size="1.27" layer="21" align="center-left">A1</text>
<text x="9.144" y="-77.47" size="1.27" layer="21" align="center-left">A31</text>
</package>
<package name="CARDEDGE_FEMALE_60PINS_PC1_ANGLEDLEFT">
<pad name="B1" x="7.62" y="-1.27" drill="0.75"/>
<pad name="B2" x="7.62" y="-3.81" drill="0.75"/>
<pad name="B3" x="7.62" y="-6.35" drill="0.75"/>
<pad name="B4" x="7.62" y="-8.89" drill="0.75"/>
<pad name="B5" x="7.62" y="-11.43" drill="0.75"/>
<pad name="B6" x="7.62" y="-13.97" drill="0.75"/>
<pad name="B7" x="7.62" y="-16.51" drill="0.75"/>
<pad name="B8" x="7.62" y="-19.05" drill="0.75"/>
<pad name="B9" x="7.62" y="-21.59" drill="0.75"/>
<pad name="B10" x="7.62" y="-24.13" drill="0.75"/>
<pad name="B11" x="7.62" y="-26.67" drill="0.75"/>
<pad name="B12" x="7.62" y="-29.21" drill="0.75"/>
<pad name="B13" x="7.62" y="-31.75" drill="0.75"/>
<pad name="B14" x="7.62" y="-34.29" drill="0.75"/>
<pad name="B15" x="7.62" y="-36.83" drill="0.75"/>
<pad name="B16" x="7.62" y="-39.37" drill="0.75"/>
<pad name="B17" x="7.62" y="-41.91" drill="0.75"/>
<pad name="B18" x="7.62" y="-44.45" drill="0.75"/>
<pad name="B19" x="7.62" y="-46.99" drill="0.75"/>
<pad name="B20" x="7.62" y="-49.53" drill="0.75"/>
<pad name="B21" x="7.62" y="-52.07" drill="0.75"/>
<pad name="B22" x="7.62" y="-54.61" drill="0.75"/>
<pad name="B23" x="7.62" y="-57.15" drill="0.75"/>
<pad name="B24" x="7.62" y="-59.69" drill="0.75"/>
<pad name="B25" x="7.62" y="-62.23" drill="0.75"/>
<pad name="B26" x="7.62" y="-64.77" drill="0.75"/>
<pad name="B27" x="7.62" y="-67.31" drill="0.75"/>
<pad name="B28" x="7.62" y="-69.85" drill="0.75"/>
<pad name="B29" x="7.62" y="-72.39" drill="0.75"/>
<pad name="B30" x="7.62" y="-74.93" drill="0.75"/>
<pad name="A1" x="2.54" y="-1.27" drill="0.75"/>
<pad name="A2" x="2.54" y="-3.81" drill="0.75"/>
<pad name="A3" x="2.54" y="-6.35" drill="0.75"/>
<pad name="A4" x="2.54" y="-8.89" drill="0.75"/>
<pad name="A5" x="2.54" y="-11.43" drill="0.75"/>
<pad name="A6" x="2.54" y="-13.97" drill="0.75"/>
<pad name="A7" x="2.54" y="-16.51" drill="0.75"/>
<pad name="A8" x="2.54" y="-19.05" drill="0.75"/>
<pad name="A9" x="2.54" y="-21.59" drill="0.75"/>
<pad name="A10" x="2.54" y="-24.13" drill="0.75"/>
<pad name="A11" x="2.54" y="-26.67" drill="0.75"/>
<pad name="A12" x="2.54" y="-29.21" drill="0.75"/>
<pad name="A13" x="2.54" y="-31.75" drill="0.75"/>
<pad name="A14" x="2.54" y="-34.29" drill="0.75"/>
<pad name="A15" x="2.54" y="-36.83" drill="0.75"/>
<pad name="A16" x="2.54" y="-39.37" drill="0.75"/>
<pad name="A17" x="2.54" y="-41.91" drill="0.75"/>
<pad name="A18" x="2.54" y="-44.45" drill="0.75"/>
<pad name="A19" x="2.54" y="-46.99" drill="0.75"/>
<pad name="A20" x="2.54" y="-49.53" drill="0.75"/>
<pad name="A21" x="2.54" y="-52.07" drill="0.75"/>
<pad name="A22" x="2.54" y="-54.61" drill="0.75"/>
<pad name="A23" x="2.54" y="-57.15" drill="0.75"/>
<pad name="A24" x="2.54" y="-59.69" drill="0.75"/>
<pad name="A25" x="2.54" y="-62.23" drill="0.75"/>
<pad name="A26" x="2.54" y="-64.77" drill="0.75"/>
<pad name="A27" x="2.54" y="-67.31" drill="0.75"/>
<pad name="A28" x="2.54" y="-69.85" drill="0.75"/>
<pad name="A29" x="2.54" y="-72.39" drill="0.75"/>
<pad name="A30" x="2.54" y="-74.93" drill="0.75"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.127" layer="21"/>
<wire x1="2.54" y1="0" x2="8.89" y2="0" width="0.127" layer="21"/>
<wire x1="8.89" y1="0" x2="8.89" y2="-76.2" width="0.127" layer="21"/>
<wire x1="8.89" y1="-76.2" x2="2.54" y2="-76.2" width="0.127" layer="21"/>
<wire x1="2.54" y1="-76.2" x2="1.27" y2="-76.2" width="0.127" layer="21"/>
<wire x1="1.27" y1="-76.2" x2="1.27" y2="0" width="0.127" layer="21"/>
<text x="11.43" y="-19.05" size="1.27" layer="21" rot="R90">&gt;NAME</text>
<text x="9.144" y="-1.27" size="1.27" layer="21" align="center-left">B1</text>
<text x="9.144" y="-74.93" size="1.27" layer="21" align="center-left">B30</text>
<text x="1.016" y="-1.27" size="1.27" layer="21" align="center-right">A1</text>
<text x="1.016" y="-74.93" size="1.27" layer="21" align="center-right">A30</text>
<rectangle x1="-11.4554" y1="-79.375" x2="2.54" y2="3.175" layer="39"/>
<rectangle x1="2.54" y1="-74.93" x2="7.62" y2="-1.27" layer="39"/>
<wire x1="2.54" y1="-76.2" x2="2.54" y2="-79.375" width="0.127" layer="21"/>
<wire x1="2.54" y1="-79.375" x2="-11.43" y2="-79.375" width="0.127" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="3.175" width="0.127" layer="21"/>
<wire x1="2.54" y1="3.175" x2="-11.43" y2="3.175" width="0.127" layer="21"/>
</package>
<package name="PC-PWR_MALE_4PINS_VERTICAL">
<pad name="1_+12V" x="1.27" y="0" drill="1.905" shape="square"/>
<pad name="2_GND" x="-3.81" y="0" drill="1.905" shape="octagon"/>
<pad name="3_GND" x="-8.89" y="0" drill="1.905" shape="octagon"/>
<pad name="4_+5V" x="-13.97" y="0" drill="1.905" shape="octagon"/>
<rectangle x1="-19.05" y1="-5.5372" x2="6.35" y2="4.1148" layer="39"/>
<wire x1="-17.78" y1="-3.81" x2="-17.78" y2="1.27" width="0.127" layer="21"/>
<wire x1="-17.78" y1="1.27" x2="-15.24" y2="3.81" width="0.127" layer="21"/>
<wire x1="-15.24" y1="3.81" x2="2.54" y2="3.81" width="0.127" layer="21"/>
<wire x1="2.54" y1="3.81" x2="5.08" y2="1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="1.27" x2="5.08" y2="-3.81" width="0.127" layer="21"/>
<wire x1="5.08" y1="-3.81" x2="-17.78" y2="-3.81" width="0.127" layer="21"/>
<text x="-17.78" y="5.08" size="1.27" layer="21">&gt;NAME</text>
<text x="-15.24" y="-6.35" size="1.27" layer="21">+5V</text>
<text x="-10.16" y="-6.35" size="1.27" layer="21">GND</text>
<text x="-5.08" y="-6.35" size="1.27" layer="21">GND</text>
<text x="0" y="-6.35" size="1.27" layer="21">+12V</text>
</package>
</packages>
<symbols>
<symbol name="CARDEDGE_FEMALE_62PINS_8BITISA">
<wire x1="0" y1="0" x2="35.56" y2="0" width="0.254" layer="94"/>
<wire x1="35.56" y1="0" x2="35.56" y2="-81.28" width="0.254" layer="94"/>
<wire x1="35.56" y1="-81.28" x2="0" y2="-81.28" width="0.254" layer="94"/>
<wire x1="0" y1="-81.28" x2="0" y2="0" width="0.254" layer="94"/>
<pin name="B1_GND" x="-5.08" y="-2.54" length="middle"/>
<pin name="B2_RESET_DRV" x="-5.08" y="-5.08" length="middle"/>
<pin name="B3_+5V" x="-5.08" y="-7.62" length="middle"/>
<pin name="B4_IRQ2" x="-5.08" y="-10.16" length="middle"/>
<pin name="B5_-5V" x="-5.08" y="-12.7" length="middle"/>
<pin name="B6_DRQ2" x="-5.08" y="-15.24" length="middle"/>
<pin name="B7_-12V" x="-5.08" y="-17.78" length="middle"/>
<pin name="B8_NO_WAITST" x="-5.08" y="-20.32" length="middle"/>
<pin name="B9_+12V" x="-5.08" y="-22.86" length="middle"/>
<pin name="B10_GND" x="-5.08" y="-25.4" length="middle"/>
<pin name="B11_MEMW" x="-5.08" y="-27.94" length="middle"/>
<pin name="B12_MEMR" x="-5.08" y="-30.48" length="middle"/>
<pin name="B13_IOW" x="-5.08" y="-33.02" length="middle"/>
<pin name="B14_IOR" x="-5.08" y="-35.56" length="middle"/>
<pin name="B15_DACK3" x="-5.08" y="-38.1" length="middle"/>
<pin name="B16_DRQ3" x="-5.08" y="-40.64" length="middle"/>
<pin name="B17_DACK1" x="-5.08" y="-43.18" length="middle"/>
<pin name="B18_DRQ1" x="-5.08" y="-45.72" length="middle"/>
<pin name="B19_REFRESH" x="-5.08" y="-48.26" length="middle"/>
<pin name="B20_CLK" x="-5.08" y="-50.8" length="middle"/>
<pin name="B21_IRQ7" x="-5.08" y="-53.34" length="middle"/>
<pin name="B22_IRQ6" x="-5.08" y="-55.88" length="middle"/>
<pin name="B23_IRQ5" x="-5.08" y="-58.42" length="middle"/>
<pin name="B24_IRQ4" x="-5.08" y="-60.96" length="middle"/>
<pin name="B25_IRQ3" x="-5.08" y="-63.5" length="middle"/>
<pin name="B26_DACK2" x="-5.08" y="-66.04" length="middle"/>
<pin name="B27_T/C" x="-5.08" y="-68.58" length="middle"/>
<pin name="B28_ALE" x="-5.08" y="-71.12" length="middle"/>
<pin name="B29_+5V" x="-5.08" y="-73.66" length="middle"/>
<pin name="B30_OSC" x="-5.08" y="-76.2" length="middle"/>
<pin name="B31_GND" x="-5.08" y="-78.74" length="middle"/>
<pin name="A1_I/O_CH_CK" x="40.64" y="-2.54" length="middle" rot="R180"/>
<pin name="A2_DATA7" x="40.64" y="-5.08" length="middle" rot="R180"/>
<pin name="A3_DATA6" x="40.64" y="-7.62" length="middle" rot="R180"/>
<pin name="A4_DATA5" x="40.64" y="-10.16" length="middle" rot="R180"/>
<pin name="A5_DATA4" x="40.64" y="-12.7" length="middle" rot="R180"/>
<pin name="A6_DATA3" x="40.64" y="-15.24" length="middle" rot="R180"/>
<pin name="A7_DATA2" x="40.64" y="-17.78" length="middle" rot="R180"/>
<pin name="A8_DATA1" x="40.64" y="-20.32" length="middle" rot="R180"/>
<pin name="A9_DATA0" x="40.64" y="-22.86" length="middle" rot="R180"/>
<pin name="A10_I/O_CH_RDY" x="40.64" y="-25.4" length="middle" rot="R180"/>
<pin name="A11_AEN" x="40.64" y="-27.94" length="middle" rot="R180"/>
<pin name="A12_ADDR19" x="40.64" y="-30.48" length="middle" rot="R180"/>
<pin name="A13_ADDR18" x="40.64" y="-33.02" length="middle" rot="R180"/>
<pin name="A14_ADDR17" x="40.64" y="-35.56" length="middle" rot="R180"/>
<pin name="A15_ADDR16" x="40.64" y="-38.1" length="middle" rot="R180"/>
<pin name="A16_ADDR15" x="40.64" y="-40.64" length="middle" rot="R180"/>
<pin name="A17_ADDR14" x="40.64" y="-43.18" length="middle" rot="R180"/>
<pin name="A18_ADDR13" x="40.64" y="-45.72" length="middle" rot="R180"/>
<pin name="A19_ADDR12" x="40.64" y="-48.26" length="middle" rot="R180"/>
<pin name="A20_ADDR11" x="40.64" y="-50.8" length="middle" rot="R180"/>
<pin name="A21_ADDR10" x="40.64" y="-53.34" length="middle" rot="R180"/>
<pin name="A22_ADDR09" x="40.64" y="-55.88" length="middle" rot="R180"/>
<pin name="A23_ADDR08" x="40.64" y="-58.42" length="middle" rot="R180"/>
<pin name="A24_ADDR07" x="40.64" y="-60.96" length="middle" rot="R180"/>
<pin name="A25_ADDR06" x="40.64" y="-63.5" length="middle" rot="R180"/>
<pin name="A26_ADDR05" x="40.64" y="-66.04" length="middle" rot="R180"/>
<pin name="A27_ADDR04" x="40.64" y="-68.58" length="middle" rot="R180"/>
<pin name="A28_ADDR03" x="40.64" y="-71.12" length="middle" rot="R180"/>
<pin name="A29_ADDR02" x="40.64" y="-73.66" length="middle" rot="R180"/>
<pin name="A30_ADDR01" x="40.64" y="-76.2" length="middle" rot="R180"/>
<pin name="A31_ADDR00" x="40.64" y="-78.74" length="middle" rot="R180"/>
<text x="5.08" y="2.54" size="1.778" layer="95">&gt;NAME</text>
</symbol>
<symbol name="CARDEDGE_FEMALE_60PINS_PC1">
<wire x1="0" y1="0" x2="35.56" y2="0" width="0.254" layer="94"/>
<wire x1="35.56" y1="0" x2="35.56" y2="-78.74" width="0.254" layer="94"/>
<wire x1="35.56" y1="-78.74" x2="0" y2="-78.74" width="0.254" layer="94"/>
<wire x1="0" y1="-78.74" x2="0" y2="0" width="0.254" layer="94"/>
<pin name="B1_GND" x="40.64" y="-2.54" length="middle" rot="R180"/>
<pin name="B2_RESETDRV" x="40.64" y="-5.08" length="middle" rot="R180"/>
<pin name="B3_GND" x="40.64" y="-7.62" length="middle" rot="R180"/>
<pin name="B4_IRQ2" x="40.64" y="-10.16" length="middle" rot="R180"/>
<pin name="B5_GND" x="40.64" y="-12.7" length="middle" rot="R180"/>
<pin name="B6_DRQ2" x="40.64" y="-15.24" length="middle" rot="R180"/>
<pin name="B7_GND" x="40.64" y="-17.78" length="middle" rot="R180"/>
<pin name="B8_GND" x="40.64" y="-20.32" length="middle" rot="R180"/>
<pin name="B9_/IOCHCK" x="40.64" y="-22.86" length="middle" rot="R180"/>
<pin name="B10_GND" x="40.64" y="-25.4" length="middle" rot="R180"/>
<pin name="B11_/BMEMW" x="40.64" y="-27.94" length="middle" rot="R180"/>
<pin name="B12_/BMEMR" x="40.64" y="-30.48" length="middle" rot="R180"/>
<pin name="B13_/BIOW" x="40.64" y="-33.02" length="middle" rot="R180"/>
<pin name="B14_/BIOR" x="40.64" y="-35.56" length="middle" rot="R180"/>
<pin name="B15_/BDACK3" x="40.64" y="-38.1" length="middle" rot="R180"/>
<pin name="B16_DRQ3" x="40.64" y="-40.64" length="middle" rot="R180"/>
<pin name="B17_/BDACK1" x="40.64" y="-43.18" length="middle" rot="R180"/>
<pin name="B18_DRQ1" x="40.64" y="-45.72" length="middle" rot="R180"/>
<pin name="B19_/BDACK0" x="40.64" y="-48.26" length="middle" rot="R180"/>
<pin name="B20_BCLK(CPU)" x="40.64" y="-50.8" length="middle" rot="R180"/>
<pin name="B21_IRQ7" x="40.64" y="-53.34" length="middle" rot="R180"/>
<pin name="B22_IRQ6" x="40.64" y="-55.88" length="middle" rot="R180"/>
<pin name="B23_IRQ5" x="40.64" y="-58.42" length="middle" rot="R180"/>
<pin name="B24_IRQ4" x="40.64" y="-60.96" length="middle" rot="R180"/>
<pin name="B25_IRQ3" x="40.64" y="-63.5" length="middle" rot="R180"/>
<pin name="B26_/BDACK2" x="40.64" y="-66.04" length="middle" rot="R180"/>
<pin name="B27_TC" x="40.64" y="-68.58" length="middle" rot="R180"/>
<pin name="B28_BALE" x="40.64" y="-71.12" length="middle" rot="R180"/>
<pin name="B29_GND" x="40.64" y="-73.66" length="middle" rot="R180"/>
<pin name="B30_BOSC" x="40.64" y="-76.2" length="middle" rot="R180"/>
<pin name="A1_BD7" x="-5.08" y="-2.54" length="middle"/>
<pin name="A2_BD6" x="-5.08" y="-5.08" length="middle"/>
<pin name="A3_BD5" x="-5.08" y="-7.62" length="middle"/>
<pin name="A4_BD4" x="-5.08" y="-10.16" length="middle"/>
<pin name="A5_BD3" x="-5.08" y="-12.7" length="middle"/>
<pin name="A6_BD2" x="-5.08" y="-15.24" length="middle"/>
<pin name="A7_BD1" x="-5.08" y="-17.78" length="middle"/>
<pin name="A8_BD0" x="-5.08" y="-20.32" length="middle"/>
<pin name="A9_IOCHRDY" x="-5.08" y="-22.86" length="middle"/>
<pin name="A10_BAEN" x="-5.08" y="-25.4" length="middle"/>
<pin name="A11_BA19" x="-5.08" y="-27.94" length="middle"/>
<pin name="A12_BA18" x="-5.08" y="-30.48" length="middle"/>
<pin name="A13_BA17" x="-5.08" y="-33.02" length="middle"/>
<pin name="A14_BA16" x="-5.08" y="-35.56" length="middle"/>
<pin name="A15_BA15" x="-5.08" y="-38.1" length="middle"/>
<pin name="A16_BA14" x="-5.08" y="-40.64" length="middle"/>
<pin name="A17_BA13" x="-5.08" y="-43.18" length="middle"/>
<pin name="A18_BA12" x="-5.08" y="-45.72" length="middle"/>
<pin name="A19_BA11" x="-5.08" y="-48.26" length="middle"/>
<pin name="A20_BA10" x="-5.08" y="-50.8" length="middle"/>
<pin name="A21_BA9" x="-5.08" y="-53.34" length="middle"/>
<pin name="A22_BA8" x="-5.08" y="-55.88" length="middle"/>
<pin name="A23_BA7" x="-5.08" y="-58.42" length="middle"/>
<pin name="A24_BA6" x="-5.08" y="-60.96" length="middle"/>
<pin name="A25_BA5" x="-5.08" y="-63.5" length="middle"/>
<pin name="A26_BA4" x="-5.08" y="-66.04" length="middle"/>
<pin name="A27_BA3" x="-5.08" y="-68.58" length="middle"/>
<pin name="A28_BA2" x="-5.08" y="-71.12" length="middle"/>
<pin name="A29_BA1" x="-5.08" y="-73.66" length="middle"/>
<pin name="A30_BA0" x="-5.08" y="-76.2" length="middle"/>
<text x="5.08" y="2.54" size="1.778" layer="95">&gt;NAME</text>
</symbol>
<symbol name="PC-PWR_MALE_4PINS">
<wire x1="20.32" y1="-2.54" x2="20.32" y2="-12.7" width="0.254" layer="94"/>
<wire x1="20.32" y1="-12.7" x2="0" y2="-12.7" width="0.254" layer="94"/>
<wire x1="0" y1="-12.7" x2="0" y2="-2.54" width="0.254" layer="94"/>
<pin name="1_+12V" x="17.78" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="2_GND" x="12.7" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="3_GND" x="7.62" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="4_+5V" x="2.54" y="-17.78" visible="pin" length="middle" rot="R90"/>
<text x="-2.54" y="-12.7" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<wire x1="0" y1="-2.54" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="17.78" y2="0" width="0.254" layer="94"/>
<wire x1="17.78" y1="0" x2="20.32" y2="-2.54" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CARDEDGE_FEMALE_62PINS_8BITISA_STRAIGHT" prefix="CN">
<description>8bit ISA connector
- for interfacing with an 8-bit ISA expansion card, i.e. this is the computer side. This is for a straight through-hole connector. Metal plate of the expansion card should go in the A1 and B1 end of the connector.</description>
<gates>
<gate name="G$1" symbol="CARDEDGE_FEMALE_62PINS_8BITISA" x="53.34" y="-22.86"/>
</gates>
<devices>
<device name="" package="CARDEDGE_FEMALE_62PINS_8BITISA_STRAIGHT">
<connects>
<connect gate="G$1" pin="A10_I/O_CH_RDY" pad="A10"/>
<connect gate="G$1" pin="A11_AEN" pad="A11"/>
<connect gate="G$1" pin="A12_ADDR19" pad="A12"/>
<connect gate="G$1" pin="A13_ADDR18" pad="A13"/>
<connect gate="G$1" pin="A14_ADDR17" pad="A14"/>
<connect gate="G$1" pin="A15_ADDR16" pad="A15"/>
<connect gate="G$1" pin="A16_ADDR15" pad="A16"/>
<connect gate="G$1" pin="A17_ADDR14" pad="A17"/>
<connect gate="G$1" pin="A18_ADDR13" pad="A18"/>
<connect gate="G$1" pin="A19_ADDR12" pad="A19"/>
<connect gate="G$1" pin="A1_I/O_CH_CK" pad="A1"/>
<connect gate="G$1" pin="A20_ADDR11" pad="A20"/>
<connect gate="G$1" pin="A21_ADDR10" pad="A21"/>
<connect gate="G$1" pin="A22_ADDR09" pad="A22"/>
<connect gate="G$1" pin="A23_ADDR08" pad="A23"/>
<connect gate="G$1" pin="A24_ADDR07" pad="A24"/>
<connect gate="G$1" pin="A25_ADDR06" pad="A25"/>
<connect gate="G$1" pin="A26_ADDR05" pad="A26"/>
<connect gate="G$1" pin="A27_ADDR04" pad="A27"/>
<connect gate="G$1" pin="A28_ADDR03" pad="A28"/>
<connect gate="G$1" pin="A29_ADDR02" pad="A29"/>
<connect gate="G$1" pin="A2_DATA7" pad="A2"/>
<connect gate="G$1" pin="A30_ADDR01" pad="A30"/>
<connect gate="G$1" pin="A31_ADDR00" pad="A31"/>
<connect gate="G$1" pin="A3_DATA6" pad="A3"/>
<connect gate="G$1" pin="A4_DATA5" pad="A4"/>
<connect gate="G$1" pin="A5_DATA4" pad="A5"/>
<connect gate="G$1" pin="A6_DATA3" pad="A6"/>
<connect gate="G$1" pin="A7_DATA2" pad="A7"/>
<connect gate="G$1" pin="A8_DATA1" pad="A8"/>
<connect gate="G$1" pin="A9_DATA0" pad="A9"/>
<connect gate="G$1" pin="B10_GND" pad="B10"/>
<connect gate="G$1" pin="B11_MEMW" pad="B11"/>
<connect gate="G$1" pin="B12_MEMR" pad="B12"/>
<connect gate="G$1" pin="B13_IOW" pad="B13"/>
<connect gate="G$1" pin="B14_IOR" pad="B14"/>
<connect gate="G$1" pin="B15_DACK3" pad="B15"/>
<connect gate="G$1" pin="B16_DRQ3" pad="B16"/>
<connect gate="G$1" pin="B17_DACK1" pad="B17"/>
<connect gate="G$1" pin="B18_DRQ1" pad="B18"/>
<connect gate="G$1" pin="B19_REFRESH" pad="B19"/>
<connect gate="G$1" pin="B1_GND" pad="B1"/>
<connect gate="G$1" pin="B20_CLK" pad="B20"/>
<connect gate="G$1" pin="B21_IRQ7" pad="B21"/>
<connect gate="G$1" pin="B22_IRQ6" pad="B22"/>
<connect gate="G$1" pin="B23_IRQ5" pad="B23"/>
<connect gate="G$1" pin="B24_IRQ4" pad="B24"/>
<connect gate="G$1" pin="B25_IRQ3" pad="B25"/>
<connect gate="G$1" pin="B26_DACK2" pad="B26"/>
<connect gate="G$1" pin="B27_T/C" pad="B27"/>
<connect gate="G$1" pin="B28_ALE" pad="B28"/>
<connect gate="G$1" pin="B29_+5V" pad="B29"/>
<connect gate="G$1" pin="B2_RESET_DRV" pad="B2"/>
<connect gate="G$1" pin="B30_OSC" pad="B30"/>
<connect gate="G$1" pin="B31_GND" pad="B31"/>
<connect gate="G$1" pin="B3_+5V" pad="B3"/>
<connect gate="G$1" pin="B4_IRQ2" pad="B4"/>
<connect gate="G$1" pin="B5_-5V" pad="B5"/>
<connect gate="G$1" pin="B6_DRQ2" pad="B6"/>
<connect gate="G$1" pin="B7_-12V" pad="B7"/>
<connect gate="G$1" pin="B8_NO_WAITST" pad="B8"/>
<connect gate="G$1" pin="B9_+12V" pad="B9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CARDEDGE_FEMALE_60PINS_PC1_ANGLEDLEFT" prefix="CN">
<description>Commodore PC1 user port connector
- for interfacing with the 60 pin "J12" card edge connector at the back of the PC1 computer. This is for an angled through-hole connector, pins pointing downwards when connected correctly to the computer.</description>
<gates>
<gate name="G$1" symbol="CARDEDGE_FEMALE_60PINS_PC1" x="53.34" y="-22.86"/>
</gates>
<devices>
<device name="" package="CARDEDGE_FEMALE_60PINS_PC1_ANGLEDLEFT">
<connects>
<connect gate="G$1" pin="A10_BAEN" pad="A10"/>
<connect gate="G$1" pin="A11_BA19" pad="A11"/>
<connect gate="G$1" pin="A12_BA18" pad="A12"/>
<connect gate="G$1" pin="A13_BA17" pad="A13"/>
<connect gate="G$1" pin="A14_BA16" pad="A14"/>
<connect gate="G$1" pin="A15_BA15" pad="A15"/>
<connect gate="G$1" pin="A16_BA14" pad="A16"/>
<connect gate="G$1" pin="A17_BA13" pad="A17"/>
<connect gate="G$1" pin="A18_BA12" pad="A18"/>
<connect gate="G$1" pin="A19_BA11" pad="A19"/>
<connect gate="G$1" pin="A1_BD7" pad="A1"/>
<connect gate="G$1" pin="A20_BA10" pad="A20"/>
<connect gate="G$1" pin="A21_BA9" pad="A21"/>
<connect gate="G$1" pin="A22_BA8" pad="A22"/>
<connect gate="G$1" pin="A23_BA7" pad="A23"/>
<connect gate="G$1" pin="A24_BA6" pad="A24"/>
<connect gate="G$1" pin="A25_BA5" pad="A25"/>
<connect gate="G$1" pin="A26_BA4" pad="A26"/>
<connect gate="G$1" pin="A27_BA3" pad="A27"/>
<connect gate="G$1" pin="A28_BA2" pad="A28"/>
<connect gate="G$1" pin="A29_BA1" pad="A29"/>
<connect gate="G$1" pin="A2_BD6" pad="A2"/>
<connect gate="G$1" pin="A30_BA0" pad="A30"/>
<connect gate="G$1" pin="A3_BD5" pad="A3"/>
<connect gate="G$1" pin="A4_BD4" pad="A4"/>
<connect gate="G$1" pin="A5_BD3" pad="A5"/>
<connect gate="G$1" pin="A6_BD2" pad="A6"/>
<connect gate="G$1" pin="A7_BD1" pad="A7"/>
<connect gate="G$1" pin="A8_BD0" pad="A8"/>
<connect gate="G$1" pin="A9_IOCHRDY" pad="A9"/>
<connect gate="G$1" pin="B10_GND" pad="B10"/>
<connect gate="G$1" pin="B11_/BMEMW" pad="B11"/>
<connect gate="G$1" pin="B12_/BMEMR" pad="B12"/>
<connect gate="G$1" pin="B13_/BIOW" pad="B13"/>
<connect gate="G$1" pin="B14_/BIOR" pad="B14"/>
<connect gate="G$1" pin="B15_/BDACK3" pad="B15"/>
<connect gate="G$1" pin="B16_DRQ3" pad="B16"/>
<connect gate="G$1" pin="B17_/BDACK1" pad="B17"/>
<connect gate="G$1" pin="B18_DRQ1" pad="B18"/>
<connect gate="G$1" pin="B19_/BDACK0" pad="B19"/>
<connect gate="G$1" pin="B1_GND" pad="B1"/>
<connect gate="G$1" pin="B20_BCLK(CPU)" pad="B20"/>
<connect gate="G$1" pin="B21_IRQ7" pad="B21"/>
<connect gate="G$1" pin="B22_IRQ6" pad="B22"/>
<connect gate="G$1" pin="B23_IRQ5" pad="B23"/>
<connect gate="G$1" pin="B24_IRQ4" pad="B24"/>
<connect gate="G$1" pin="B25_IRQ3" pad="B25"/>
<connect gate="G$1" pin="B26_/BDACK2" pad="B26"/>
<connect gate="G$1" pin="B27_TC" pad="B27"/>
<connect gate="G$1" pin="B28_BALE" pad="B28"/>
<connect gate="G$1" pin="B29_GND" pad="B29"/>
<connect gate="G$1" pin="B2_RESETDRV" pad="B2"/>
<connect gate="G$1" pin="B30_BOSC" pad="B30"/>
<connect gate="G$1" pin="B3_GND" pad="B3"/>
<connect gate="G$1" pin="B4_IRQ2" pad="B4"/>
<connect gate="G$1" pin="B5_GND" pad="B5"/>
<connect gate="G$1" pin="B6_DRQ2" pad="B6"/>
<connect gate="G$1" pin="B7_GND" pad="B7"/>
<connect gate="G$1" pin="B8_GND" pad="B8"/>
<connect gate="G$1" pin="B9_/IOCHCK" pad="B9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PC-PWR_MALE_4PINS_VERTICAL" prefix="CN">
<description>Connector for old-style internal PC power
- PCB mountable 4 pin connector by TE Connectivity. Often (incorrectly?) referred to as a Molex connector. Used to power internal PC components such as hard drives and cdrom drives, before the use of SATA style power connectors became common. This connector is Mouser no. 571-3502111.</description>
<gates>
<gate name="G$1" symbol="PC-PWR_MALE_4PINS" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PC-PWR_MALE_4PINS_VERTICAL">
<connects>
<connect gate="G$1" pin="1_+12V" pad="1_+12V"/>
<connect gate="G$1" pin="2_GND" pad="2_GND"/>
<connect gate="G$1" pin="3_GND" pad="3_GND"/>
<connect gate="G$1" pin="4_+5V" pad="4_+5V"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0.3048" drill="0">
<clearance class="0" value="0.0762"/>
</class>
<class number="1" name="vcc" width="0.4064" drill="0">
<clearance class="1" value="0.0762"/>
</class>
</classes>
<parts>
<part name="PC/XT" library="Thomas" deviceset="CARDEDGE_FEMALE_62PINS_8BITISA_STRAIGHT" device=""/>
<part name="PC1" library="Thomas" deviceset="CARDEDGE_FEMALE_60PINS_PC1_ANGLEDLEFT" device=""/>
<part name="PWR_IN" library="Thomas" deviceset="PC-PWR_MALE_4PINS_VERTICAL" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="PC/XT" gate="G$1" x="15.24" y="78.74" smashed="yes">
<attribute name="NAME" x="20.32" y="81.28" size="1.778" layer="95"/>
</instance>
<instance part="PC1" gate="G$1" x="73.66" y="76.2" smashed="yes">
<attribute name="NAME" x="78.74" y="78.74" size="1.778" layer="95"/>
</instance>
<instance part="PWR_IN" gate="G$1" x="-45.72" y="68.58" smashed="yes" rot="R90">
<attribute name="NAME" x="-33.02" y="66.04" size="1.778" layer="95" rot="R180"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="DATA7" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="A2_DATA7"/>
<wire x1="55.88" y1="73.66" x2="68.58" y2="73.66" width="0.1524" layer="91"/>
<pinref part="PC1" gate="G$1" pin="A1_BD7"/>
</segment>
</net>
<net name="DATA6" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="A3_DATA6"/>
<pinref part="PC1" gate="G$1" pin="A2_BD6"/>
<wire x1="55.88" y1="71.12" x2="68.58" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DATA5" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="A4_DATA5"/>
<pinref part="PC1" gate="G$1" pin="A3_BD5"/>
<wire x1="55.88" y1="68.58" x2="68.58" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DATA4" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="A5_DATA4"/>
<pinref part="PC1" gate="G$1" pin="A4_BD4"/>
<wire x1="55.88" y1="66.04" x2="68.58" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DATA3" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="A6_DATA3"/>
<pinref part="PC1" gate="G$1" pin="A5_BD3"/>
<wire x1="55.88" y1="63.5" x2="68.58" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DATA2" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="A7_DATA2"/>
<pinref part="PC1" gate="G$1" pin="A6_BD2"/>
<wire x1="55.88" y1="60.96" x2="68.58" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DATA1" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="A8_DATA1"/>
<pinref part="PC1" gate="G$1" pin="A7_BD1"/>
<wire x1="55.88" y1="58.42" x2="68.58" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DATA0" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="A9_DATA0"/>
<pinref part="PC1" gate="G$1" pin="A8_BD0"/>
<wire x1="55.88" y1="55.88" x2="68.58" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="I/O_CH_RDY" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="A10_I/O_CH_RDY"/>
<pinref part="PC1" gate="G$1" pin="A9_IOCHRDY"/>
<wire x1="55.88" y1="53.34" x2="68.58" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AEN" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="A11_AEN"/>
<pinref part="PC1" gate="G$1" pin="A10_BAEN"/>
<wire x1="55.88" y1="50.8" x2="68.58" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ADDR19" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="A12_ADDR19"/>
<pinref part="PC1" gate="G$1" pin="A11_BA19"/>
<wire x1="55.88" y1="48.26" x2="68.58" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ADDR18" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="A13_ADDR18"/>
<pinref part="PC1" gate="G$1" pin="A12_BA18"/>
<wire x1="55.88" y1="45.72" x2="68.58" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ADDR17" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="A14_ADDR17"/>
<pinref part="PC1" gate="G$1" pin="A13_BA17"/>
<wire x1="55.88" y1="43.18" x2="68.58" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ADDR16" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="A15_ADDR16"/>
<pinref part="PC1" gate="G$1" pin="A14_BA16"/>
<wire x1="55.88" y1="40.64" x2="68.58" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ADDR15" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="A16_ADDR15"/>
<pinref part="PC1" gate="G$1" pin="A15_BA15"/>
<wire x1="55.88" y1="38.1" x2="68.58" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ADDR14" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="A17_ADDR14"/>
<pinref part="PC1" gate="G$1" pin="A16_BA14"/>
<wire x1="55.88" y1="35.56" x2="68.58" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ADDR13" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="A18_ADDR13"/>
<pinref part="PC1" gate="G$1" pin="A17_BA13"/>
<wire x1="55.88" y1="33.02" x2="68.58" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ADDR12" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="A19_ADDR12"/>
<pinref part="PC1" gate="G$1" pin="A18_BA12"/>
<wire x1="55.88" y1="30.48" x2="68.58" y2="30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ADDR11" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="A20_ADDR11"/>
<pinref part="PC1" gate="G$1" pin="A19_BA11"/>
<wire x1="55.88" y1="27.94" x2="68.58" y2="27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ADDR10" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="A21_ADDR10"/>
<pinref part="PC1" gate="G$1" pin="A20_BA10"/>
<wire x1="55.88" y1="25.4" x2="68.58" y2="25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ADDR09" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="A22_ADDR09"/>
<pinref part="PC1" gate="G$1" pin="A21_BA9"/>
<wire x1="55.88" y1="22.86" x2="68.58" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ADDR08" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="A23_ADDR08"/>
<pinref part="PC1" gate="G$1" pin="A22_BA8"/>
<wire x1="55.88" y1="20.32" x2="68.58" y2="20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ADDR07" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="A24_ADDR07"/>
<pinref part="PC1" gate="G$1" pin="A23_BA7"/>
<wire x1="55.88" y1="17.78" x2="68.58" y2="17.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ADDR06" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="A25_ADDR06"/>
<pinref part="PC1" gate="G$1" pin="A24_BA6"/>
<wire x1="55.88" y1="15.24" x2="68.58" y2="15.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ADDR05" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="A26_ADDR05"/>
<pinref part="PC1" gate="G$1" pin="A25_BA5"/>
<wire x1="55.88" y1="12.7" x2="68.58" y2="12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ADDR04" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="A27_ADDR04"/>
<pinref part="PC1" gate="G$1" pin="A26_BA4"/>
<wire x1="55.88" y1="10.16" x2="68.58" y2="10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ADDR03" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="A28_ADDR03"/>
<pinref part="PC1" gate="G$1" pin="A27_BA3"/>
<wire x1="55.88" y1="7.62" x2="68.58" y2="7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ADDR02" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="A29_ADDR02"/>
<pinref part="PC1" gate="G$1" pin="A28_BA2"/>
<wire x1="55.88" y1="5.08" x2="68.58" y2="5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ADDR01" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="A30_ADDR01"/>
<pinref part="PC1" gate="G$1" pin="A29_BA1"/>
<wire x1="55.88" y1="2.54" x2="68.58" y2="2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ADDR00" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="A31_ADDR00"/>
<pinref part="PC1" gate="G$1" pin="A30_BA0"/>
<wire x1="55.88" y1="0" x2="68.58" y2="0" width="0.1524" layer="91"/>
</segment>
</net>
<net name="I/O_CH_CK" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="A1_I/O_CH_CK"/>
<wire x1="55.88" y1="76.2" x2="63.5" y2="76.2" width="0.1524" layer="91"/>
<wire x1="63.5" y1="76.2" x2="63.5" y2="83.82" width="0.1524" layer="91"/>
<wire x1="63.5" y1="83.82" x2="116.84" y2="83.82" width="0.1524" layer="91"/>
<wire x1="116.84" y1="83.82" x2="116.84" y2="53.34" width="0.1524" layer="91"/>
<pinref part="PC1" gate="G$1" pin="B9_/IOCHCK"/>
<wire x1="116.84" y1="53.34" x2="114.3" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RESET_DRV" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="B2_RESET_DRV"/>
<wire x1="10.16" y1="73.66" x2="5.08" y2="73.66" width="0.1524" layer="91"/>
<wire x1="5.08" y1="73.66" x2="5.08" y2="88.9" width="0.1524" layer="91"/>
<wire x1="5.08" y1="88.9" x2="121.92" y2="88.9" width="0.1524" layer="91"/>
<wire x1="121.92" y1="88.9" x2="121.92" y2="71.12" width="0.1524" layer="91"/>
<pinref part="PC1" gate="G$1" pin="B2_RESETDRV"/>
<wire x1="121.92" y1="71.12" x2="114.3" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IRQ2" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="B4_IRQ2"/>
<wire x1="10.16" y1="68.58" x2="2.54" y2="68.58" width="0.1524" layer="91"/>
<wire x1="2.54" y1="68.58" x2="2.54" y2="91.44" width="0.1524" layer="91"/>
<wire x1="2.54" y1="91.44" x2="124.46" y2="91.44" width="0.1524" layer="91"/>
<wire x1="124.46" y1="91.44" x2="124.46" y2="66.04" width="0.1524" layer="91"/>
<pinref part="PC1" gate="G$1" pin="B4_IRQ2"/>
<wire x1="124.46" y1="66.04" x2="114.3" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="B6_DRQ2"/>
<wire x1="10.16" y1="63.5" x2="0" y2="63.5" width="0.1524" layer="91"/>
<wire x1="0" y1="63.5" x2="0" y2="93.98" width="0.1524" layer="91"/>
<wire x1="0" y1="93.98" x2="127" y2="93.98" width="0.1524" layer="91"/>
<wire x1="127" y1="93.98" x2="127" y2="60.96" width="0.1524" layer="91"/>
<pinref part="PC1" gate="G$1" pin="B6_DRQ2"/>
<wire x1="127" y1="60.96" x2="114.3" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="B10_GND"/>
<wire x1="10.16" y1="53.34" x2="7.62" y2="53.34" width="0.1524" layer="91"/>
<wire x1="7.62" y1="53.34" x2="-2.54" y2="53.34" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="53.34" x2="-2.54" y2="96.52" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="96.52" x2="129.54" y2="96.52" width="0.1524" layer="91"/>
<wire x1="129.54" y1="96.52" x2="129.54" y2="50.8" width="0.1524" layer="91"/>
<pinref part="PC1" gate="G$1" pin="B10_GND"/>
<wire x1="129.54" y1="50.8" x2="119.38" y2="50.8" width="0.1524" layer="91"/>
<pinref part="PC/XT" gate="G$1" pin="B1_GND"/>
<wire x1="119.38" y1="50.8" x2="116.84" y2="50.8" width="0.1524" layer="91"/>
<wire x1="116.84" y1="50.8" x2="114.3" y2="50.8" width="0.1524" layer="91"/>
<wire x1="10.16" y1="76.2" x2="7.62" y2="76.2" width="0.1524" layer="91"/>
<wire x1="7.62" y1="76.2" x2="7.62" y2="86.36" width="0.1524" layer="91"/>
<wire x1="7.62" y1="86.36" x2="119.38" y2="86.36" width="0.1524" layer="91"/>
<wire x1="119.38" y1="86.36" x2="119.38" y2="73.66" width="0.1524" layer="91"/>
<pinref part="PC1" gate="G$1" pin="B1_GND"/>
<wire x1="119.38" y1="73.66" x2="114.3" y2="73.66" width="0.1524" layer="91"/>
<wire x1="7.62" y1="76.2" x2="7.62" y2="53.34" width="0.1524" layer="91"/>
<junction x="7.62" y="76.2"/>
<junction x="7.62" y="53.34"/>
<wire x1="7.62" y1="53.34" x2="7.62" y2="0" width="0.1524" layer="91"/>
<pinref part="PC/XT" gate="G$1" pin="B31_GND"/>
<wire x1="7.62" y1="0" x2="10.16" y2="0" width="0.1524" layer="91"/>
<wire x1="119.38" y1="73.66" x2="119.38" y2="68.58" width="0.1524" layer="91"/>
<junction x="119.38" y="73.66"/>
<pinref part="PC1" gate="G$1" pin="B3_GND"/>
<wire x1="119.38" y1="68.58" x2="114.3" y2="68.58" width="0.1524" layer="91"/>
<wire x1="119.38" y1="68.58" x2="119.38" y2="63.5" width="0.1524" layer="91"/>
<junction x="119.38" y="68.58"/>
<pinref part="PC1" gate="G$1" pin="B5_GND"/>
<wire x1="119.38" y1="63.5" x2="114.3" y2="63.5" width="0.1524" layer="91"/>
<wire x1="119.38" y1="63.5" x2="119.38" y2="58.42" width="0.1524" layer="91"/>
<junction x="119.38" y="63.5"/>
<pinref part="PC1" gate="G$1" pin="B7_GND"/>
<wire x1="119.38" y1="58.42" x2="114.3" y2="58.42" width="0.1524" layer="91"/>
<wire x1="119.38" y1="58.42" x2="119.38" y2="55.88" width="0.1524" layer="91"/>
<junction x="119.38" y="58.42"/>
<pinref part="PC1" gate="G$1" pin="B8_GND"/>
<wire x1="119.38" y1="55.88" x2="114.3" y2="55.88" width="0.1524" layer="91"/>
<wire x1="119.38" y1="55.88" x2="119.38" y2="50.8" width="0.1524" layer="91"/>
<junction x="119.38" y="55.88"/>
<junction x="119.38" y="50.8"/>
<pinref part="PC1" gate="G$1" pin="B29_GND"/>
<wire x1="114.3" y1="2.54" x2="116.84" y2="2.54" width="0.1524" layer="91"/>
<wire x1="116.84" y1="2.54" x2="116.84" y2="50.8" width="0.1524" layer="91"/>
<junction x="116.84" y="50.8"/>
</segment>
</net>
<net name="MEMW" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="B11_MEMW"/>
<wire x1="10.16" y1="50.8" x2="-5.08" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="50.8" x2="-5.08" y2="99.06" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="99.06" x2="132.08" y2="99.06" width="0.1524" layer="91"/>
<wire x1="132.08" y1="99.06" x2="132.08" y2="48.26" width="0.1524" layer="91"/>
<pinref part="PC1" gate="G$1" pin="B11_/BMEMW"/>
<wire x1="132.08" y1="48.26" x2="114.3" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MEMR" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="B12_MEMR"/>
<wire x1="10.16" y1="48.26" x2="-7.62" y2="48.26" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="48.26" x2="-7.62" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="101.6" x2="134.62" y2="101.6" width="0.1524" layer="91"/>
<wire x1="134.62" y1="101.6" x2="134.62" y2="45.72" width="0.1524" layer="91"/>
<pinref part="PC1" gate="G$1" pin="B12_/BMEMR"/>
<wire x1="134.62" y1="45.72" x2="114.3" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OSC" class="0">
<segment>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="5.08" y1="-5.08" x2="116.84" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="116.84" y1="-5.08" x2="116.84" y2="0" width="0.1524" layer="91"/>
<pinref part="PC1" gate="G$1" pin="B30_BOSC"/>
<wire x1="116.84" y1="0" x2="114.3" y2="0" width="0.1524" layer="91"/>
<pinref part="PC/XT" gate="G$1" pin="B30_OSC"/>
<wire x1="5.08" y1="2.54" x2="10.16" y2="2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ALE" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="B28_ALE"/>
<wire x1="10.16" y1="7.62" x2="2.54" y2="7.62" width="0.1524" layer="91"/>
<wire x1="2.54" y1="7.62" x2="2.54" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="2.54" y1="-7.62" x2="119.38" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="119.38" y1="-7.62" x2="119.38" y2="5.08" width="0.1524" layer="91"/>
<pinref part="PC1" gate="G$1" pin="B28_BALE"/>
<wire x1="119.38" y1="5.08" x2="114.3" y2="5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="T/C" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="B27_T/C"/>
<wire x1="10.16" y1="10.16" x2="0" y2="10.16" width="0.1524" layer="91"/>
<wire x1="0" y1="10.16" x2="0" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="0" y1="-10.16" x2="121.92" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="121.92" y1="-10.16" x2="121.92" y2="7.62" width="0.1524" layer="91"/>
<pinref part="PC1" gate="G$1" pin="B27_TC"/>
<wire x1="121.92" y1="7.62" x2="114.3" y2="7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DACK2" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="B26_DACK2"/>
<wire x1="10.16" y1="12.7" x2="-2.54" y2="12.7" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="12.7" x2="-2.54" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="-12.7" x2="124.46" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="124.46" y1="-12.7" x2="124.46" y2="10.16" width="0.1524" layer="91"/>
<pinref part="PC1" gate="G$1" pin="B26_/BDACK2"/>
<wire x1="124.46" y1="10.16" x2="114.3" y2="10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IRQ3" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="B25_IRQ3"/>
<wire x1="10.16" y1="15.24" x2="-5.08" y2="15.24" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="15.24" x2="-5.08" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="-15.24" x2="127" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="127" y1="-15.24" x2="127" y2="12.7" width="0.1524" layer="91"/>
<pinref part="PC1" gate="G$1" pin="B25_IRQ3"/>
<wire x1="127" y1="12.7" x2="114.3" y2="12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IRQ4" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="B24_IRQ4"/>
<wire x1="10.16" y1="17.78" x2="-7.62" y2="17.78" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="17.78" x2="-7.62" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="-17.78" x2="129.54" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="129.54" y1="-17.78" x2="129.54" y2="15.24" width="0.1524" layer="91"/>
<pinref part="PC1" gate="G$1" pin="B24_IRQ4"/>
<wire x1="129.54" y1="15.24" x2="114.3" y2="15.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IRQ5" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="B23_IRQ5"/>
<wire x1="10.16" y1="20.32" x2="-10.16" y2="20.32" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="20.32" x2="-10.16" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="-20.32" x2="132.08" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="132.08" y1="-20.32" x2="132.08" y2="17.78" width="0.1524" layer="91"/>
<pinref part="PC1" gate="G$1" pin="B23_IRQ5"/>
<wire x1="132.08" y1="17.78" x2="114.3" y2="17.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IRQ6" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="B22_IRQ6"/>
<wire x1="10.16" y1="22.86" x2="-12.7" y2="22.86" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="22.86" x2="-12.7" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="-22.86" x2="134.62" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="134.62" y1="-22.86" x2="134.62" y2="20.32" width="0.1524" layer="91"/>
<pinref part="PC1" gate="G$1" pin="B22_IRQ6"/>
<wire x1="134.62" y1="20.32" x2="114.3" y2="20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IRQ7" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="B21_IRQ7"/>
<wire x1="10.16" y1="25.4" x2="-15.24" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="25.4" x2="-15.24" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="-25.4" x2="137.16" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="137.16" y1="-25.4" x2="137.16" y2="22.86" width="0.1524" layer="91"/>
<pinref part="PC1" gate="G$1" pin="B21_IRQ7"/>
<wire x1="137.16" y1="22.86" x2="114.3" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IOW" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="B13_IOW"/>
<wire x1="10.16" y1="45.72" x2="-10.16" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="45.72" x2="-10.16" y2="104.14" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="104.14" x2="137.16" y2="104.14" width="0.1524" layer="91"/>
<wire x1="137.16" y1="104.14" x2="137.16" y2="43.18" width="0.1524" layer="91"/>
<pinref part="PC1" gate="G$1" pin="B13_/BIOW"/>
<wire x1="137.16" y1="43.18" x2="114.3" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IOR" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="B14_IOR"/>
<wire x1="10.16" y1="43.18" x2="-12.7" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="43.18" x2="-12.7" y2="106.68" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="106.68" x2="139.7" y2="106.68" width="0.1524" layer="91"/>
<wire x1="139.7" y1="106.68" x2="139.7" y2="40.64" width="0.1524" layer="91"/>
<pinref part="PC1" gate="G$1" pin="B14_/BIOR"/>
<wire x1="139.7" y1="40.64" x2="114.3" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DACK3" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="B15_DACK3"/>
<wire x1="10.16" y1="40.64" x2="-15.24" y2="40.64" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="40.64" x2="-15.24" y2="109.22" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="109.22" x2="142.24" y2="109.22" width="0.1524" layer="91"/>
<wire x1="142.24" y1="109.22" x2="142.24" y2="38.1" width="0.1524" layer="91"/>
<pinref part="PC1" gate="G$1" pin="B15_/BDACK3"/>
<wire x1="142.24" y1="38.1" x2="114.3" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DRQ3" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="B16_DRQ3"/>
<wire x1="10.16" y1="38.1" x2="-17.78" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="38.1" x2="-17.78" y2="111.76" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="111.76" x2="144.78" y2="111.76" width="0.1524" layer="91"/>
<wire x1="144.78" y1="111.76" x2="144.78" y2="35.56" width="0.1524" layer="91"/>
<pinref part="PC1" gate="G$1" pin="B16_DRQ3"/>
<wire x1="144.78" y1="35.56" x2="114.3" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CLK" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="B20_CLK"/>
<wire x1="10.16" y1="27.94" x2="-17.78" y2="27.94" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="27.94" x2="-17.78" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="-27.94" x2="139.7" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="139.7" y1="-27.94" x2="139.7" y2="25.4" width="0.1524" layer="91"/>
<pinref part="PC1" gate="G$1" pin="B20_BCLK(CPU)"/>
<wire x1="139.7" y1="25.4" x2="114.3" y2="25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DRQ1" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="B18_DRQ1"/>
<wire x1="10.16" y1="33.02" x2="-20.32" y2="33.02" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="33.02" x2="-20.32" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="-30.48" x2="142.24" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="142.24" y1="-30.48" x2="142.24" y2="30.48" width="0.1524" layer="91"/>
<pinref part="PC1" gate="G$1" pin="B18_DRQ1"/>
<wire x1="142.24" y1="30.48" x2="114.3" y2="30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DACK1" class="0">
<segment>
<pinref part="PC/XT" gate="G$1" pin="B17_DACK1"/>
<wire x1="10.16" y1="35.56" x2="-22.86" y2="35.56" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="35.56" x2="-22.86" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="-33.02" x2="144.78" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="144.78" y1="-33.02" x2="144.78" y2="33.02" width="0.1524" layer="91"/>
<pinref part="PC1" gate="G$1" pin="B17_/BDACK1"/>
<wire x1="144.78" y1="33.02" x2="114.3" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+5V" class="1">
<segment>
<pinref part="PC/XT" gate="G$1" pin="B3_+5V"/>
<wire x1="10.16" y1="71.12" x2="-27.94" y2="71.12" width="0.1524" layer="91"/>
<pinref part="PWR_IN" gate="G$1" pin="4_+5V"/>
<pinref part="PC/XT" gate="G$1" pin="B29_+5V"/>
<wire x1="10.16" y1="5.08" x2="-27.94" y2="5.08" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="5.08" x2="-27.94" y2="71.12" width="0.1524" layer="91"/>
<junction x="-27.94" y="71.12"/>
</segment>
</net>
<net name="+12V" class="1">
<segment>
<pinref part="PC/XT" gate="G$1" pin="B9_+12V"/>
<wire x1="10.16" y1="55.88" x2="-20.32" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="55.88" x2="-20.32" y2="86.36" width="0.1524" layer="91"/>
<pinref part="PWR_IN" gate="G$1" pin="1_+12V"/>
<wire x1="-20.32" y1="86.36" x2="-27.94" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
