# PC1-XT

<a href="https://autumnhippo.com/products/pc1-xt"><img align="right" src="https://gitlab.com/autumnhippo/pc1-xt/-/raw/master/media/buy.png"></a>

Hardware adapter allowing the Commodore PC-1 computer to use PC/XT expansion cards
(8-bit ISA).

The PC-1 was a very small IBM PC compatible computer made by Commodore. So small
in fact, that it doesn't have any PC/XT expansion slots like most other
IBM "clones". It does, however, have its own expansion port, a simple card-edge
connector on the back of the computer which, to a degree, is compatible with the
PC/XT slot (which was later typically referred to as the 8-bit part of the ISA slot).

The PC1-XT adapter allows you to use a PC/XT/8-bit ISA expansion card with your
Commodore PC-1. Confirmed to work with this adapter is the
[Glitch Works XT-IDE](https://www.glitchwrks.com/xt-ide)
expansion card, letting you attach an IDE harddisk, or (through an IDE-CF adapter),
a CF memory card acting as a hard drive. The PC-1's power supply only delivers
+5V and +12V, however, so any PC/XT/ISA card requiring -5V or -12V will not work.
Also, the later 16-bit ISA cards (with larger connectors) will not work.

This project was heavily inspired by [MIGs Yesterchips, episode #056](https://youtu.be/CdsUfJHtgN0) (link in German),
and is used with permission.

![Board](https://gitlab.com/autumnhippo/pc1-xt/-/raw/master/media/board.jpg "Board")

## How to make your own

These are the Eagle files to use if you want to print your own PCB for the
PC1-XT adapter. Also provided are the 'gerber' files, ready to send to your favourite
PCB manufacturing company. In addition to the PCB, you will also need:

* 1 × 60 pin (2×30) female board edge connector, 2.54 mm pitch, angled (for the PC-1 end)
* 1 × 62 pin (2×31) female board edge connector, 2.54 mm pitch, vertical or angled (for the PC/XT card end)
* 1 × internal 4-pin PC power cable Y-splitter, for harddrives and 5.25 devices, 1 male to 2 female (often called a Molex power splitter, even though that might not be 100% correct)
* 1 × TE male 4 pin connector, PCB through-hole mount (Mouser no. 571-3502111)
* Basic soldering skills

In case you are having trouble sourcing the correct card-edge connectors,
[I sell them separately](https://autumnhippo.com/products/card-edge-connectors-60-62-pins).
Alternatively, you can get the complete [PC1-XT adapter](https://autumnhippo.com/products/pc1-xt)
as a kit or fully assembled and ready-to-use.

## Assembly instructions

The PC1-XT adapter is not very difficult to assemble. There are only connectors to
solder, and they are all through-hole, so even the lightly experienced solderer should
not come across too many problems with this project.

Align all pins of the card-edge connector with the holes in the PCB. If some of the pins have become
slightly bent, you may want to gently straighten them out with a pair of pliers or similar first
to avoid them becoming even more bent when inserting the connector into the PCB.

Make sure that the vertical card-edge connector sits flush against the board, and that
the angled connector is indeed sitting in a 90-degree angle before soldering. Also,
take note of the correct orientation of the power connector as marked on the board.

## Usage

Open your Commodore PC1 computer (after unplugging it). Locate and unplug the power cable
going from the power supply unit to the diskette drive. Connect the power splitter to the
cable, and connect one of the split ends back to the diskette drive.

![Power splitter](https://gitlab.com/autumnhippo/pc1-xt/-/raw/master/media/powersplitter.jpg "Power splitter")

Let the other split
end out of the computer case through the expansion port opening in the back. You might have
to loosen the motherboard and the re-seat it after, for the power connector to fit through.

![Adapter installed](https://gitlab.com/autumnhippo/pc1-xt/-/raw/master/media/installed.jpg "Adapter installed")

Close up your PC1 computer case, and connect the PC1-XT to the expansion port and supply it
with power throug the power splitter sticking out the expansion port hole.

![Card installed](https://gitlab.com/autumnhippo/pc1-xt/-/raw/master/media/installedcard.jpg "Card installed")

Attach your favourite XT extension card to the PC1-XT adapter. **EXTREMELY IMPORTANT:**
Double check (and triple check!) the orientation of your extension card! You do not want
to insert it into the adapter the wrong way around. The back end of the card must point to
the left when looking towards the *front* of the computer. Please take note of the marking
and text on the PC1-XT adapter for easy reference.

## PCB measurements

                      78.8 mm
                 ├───────────────┤
               ┬ ┌───────────────┐
               │ │ o         o   │ ┬
     PC1 SIDE  │ │               │ │  CARD SIDE
               │ │               │ │
               │ │    PC1-XT     │ │
       88.9 mm │ │               │ │ 82 mm
               │ │               │ │
               │ │               │ │
               │ │               │ │
               │ │ o         o   │ ┴
               ┴ └───────────────┘
                 │ ├─────────┤   │
                 ├─┤  54 mm  ├───┤
                8.8 mm       16 mm
      
         Hole diametre: 3 mm
           (fits M3 screws)


